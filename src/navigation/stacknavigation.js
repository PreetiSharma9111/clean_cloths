import React from 'react';
import {createStackNavigator} from '@react-navigation/stack' 
const Stack=createStackNavigator();
  import Splash from '../screens/Splash';
 import LoginPage from '../screens/LoginPage';
 import OtpPage from '../screens/OtpPage';

  import SelectAddress from '../screens/SelectAddress';
  import Map from '../screens/Map';
  import BottomStack from '../navigation/bottomstack';



const MainStack=()=>{
    return(
        <Stack.Navigator>
                <Stack.Screen name='Splash' component={Splash} options={{headerShown:false}} ></Stack.Screen>   
                <Stack.Screen name='LoginPage' component={LoginPage}></Stack.Screen>
               <Stack.Screen name='OtpPage' component={OtpPage}></Stack.Screen> 
                    
               <Stack.Screen name='SelectAddress' component={SelectAddress}></Stack.Screen> 
               <Stack.Screen name='Map' component={Map}></Stack.Screen> 
               <Stack.Screen name='BottomStack' component={BottomStack}></Stack.Screen>   
        </Stack.Navigator>
    )
}
export default MainStack;