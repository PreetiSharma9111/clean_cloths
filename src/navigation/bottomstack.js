import React from 'react';
import {Image} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import Home from '../BottomTabScreens/Home';
import Orders from '../BottomTabScreens/Orders';
import Schedule from '../BottomTabScreens/Schedule';
import Pricing from '../BottomTabScreens/Pricing';
import Account from '../BottomTabScreens/Account';
const Tab1=createBottomTabNavigator();
const BottomStack=()=>{
    return(
        <Tab1.Navigator>
        <Tab1.Screen name='Home' component={Home} options={{
            tabBarLabel: 'Home',
            tabBarIcon: (focused, tintColor) => (
              <Image style={{ width: 30, height: 30 }} 
                     source={require('../assets/home.png')} />
            )
            }}/>
        <Tab1.Screen name='Orders' component={Orders} options={{
            tabBarLabel: 'Orders',
            tabBarIcon: (focused, tintColor) => (
              <Image style={{ width: 30, height: 30 }} 
                     source={require('../assets/orders.png')} />
            )
            }}/>
        <Tab1.Screen name='Schedule' component={Schedule} options={{
            tabBarLabel: 'Schedule',
            tabBarIcon: (focused, tintColor) => (
              <Image style={{ width: 30, height: 30 }} 
                     source={require('../assets/schedule.png')} />
            )
            }}/>
        <Tab1.Screen name='Pricing' component={Pricing} options={{
            tabBarLabel: 'Pricing',
            tabBarIcon: (focused, tintColor) => (
              <Image style={{ width: 30, height: 30 }} 
                     source={require('../assets/pricing.png')} />
            )
            }}/>
        <Tab1.Screen name='Account' component={Account} options={{
            tabBarLabel: 'Account',
            tabBarIcon: (focused, tintColor) => (
              <Image style={{ width: 30, height: 30 }} 
                     source={require('../assets/account.png')} />
            )
            }} />
        </Tab1.Navigator>
        
    );
}
export default BottomStack;