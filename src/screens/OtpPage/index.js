import React from 'react';
import { View, Text, TouchableOpacity, TextInput, } from 'react-native';
import { useRef, useState } from 'react';
const OtpPage = ({ navigation }) => {
    const [opt1, setOtp1] = useState('')
    const [opt2, setOtp2] = useState('')
    const [opt3, setOtp3] = useState('')
    const [opt4, setOtp4] = useState('')
    const ref_input1 = useRef();
    const ref_input2 = useRef();
    const ref_input3 = useRef();
    const ref_input4 = useRef();
    const Varify = () => {
        navigation.navigate('BottomStack')

    }
    return (
        <View style={{ height: '100%', width: '100%' }}>

            <View style={{ marginTop: '50%', flexDirection: 'row', marginHorizontal: '25%', justifyContent: 'space-between' }}>

                <TextInput placeholder=' ' returnKeyType="next" keyboardType="numeric" maxLength={1} ref={ref_input1}
                    onChangeText={otp1 => {
                        setOtp1(otp1)
                        if (otp1) ref_input2.current.focus();
                    }} style={{ borderBottomWidth: 1 }}></TextInput>

                <TextInput placeholder=' ' returnKeyType='next' keyboardType='numeric' maxLength={1} ref={ref_input2}
                    onChangeText={opt2 => {
                        setOtp2(opt2)
                        if (opt2) ref_input3.current.focus();
                    }}
                    style={{ borderBottomWidth: 1 }}></TextInput>

                <TextInput placeholder=' ' returnKeyType='next' keyboardType='numeric' maxLength={1} ref={ref_input3}
                    onChangeText={opt3 => {
                        setOtp3(opt3)
                        if (opt3) ref_input4.current.focus();
                    }}
                    style={{ borderBottomWidth: 1 }}></TextInput>
                <TextInput placeholder=' ' returnKeyType='next' keyboardType='numeric' maxLength={1} ref={ref_input4}
                onChangeText={opt4=>{
                    setOtp4(opt4)
                    if(opt4) ref_input4.current.focus();
                }}
                style={{ borderBottomWidth: 1 }}></TextInput>
            </View>
            <TouchableOpacity onPress={() => Varify()}
                style={{
                    marginTop: '10%', alignItems: 'center', backgroundColor: '#317773', width: '50%', height: '10%',
                    justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:12}}>
                <Text style={{ fontSize: 23, color: 'white' }}>Varify</Text>
            </TouchableOpacity>
        </View>
    )
}
export default OtpPage;