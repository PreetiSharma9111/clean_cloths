import React from 'react';
import { useEffect } from 'react';
import { View, Image } from 'react-native';
const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(function () {
            // navigation.navigate('LoginPage')
            navigation.navigate('BottomStack')
        }, 1000);
    })

    return (
        <View style={{ backgroundColor: '#317773', height: '100%', width: '100%' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: '45%' }}>
                <Image source={require('../../assets/splash.png')}
                    style={{ height: '52%', width: '36%' }}></Image>
            </View>
        </View>

    )
}
export default Splash;