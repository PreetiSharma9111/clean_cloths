import React from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
const LoginPage = ({ navigation }) => {
    const Login = () => {
        navigation.navigate('OtpPage')
    }
    return (
        <View style={{ height: '100%', width: '100%' }}>

            <View style={{ marginTop: '50%', marginHorizontal: '5%' }}>

                <Text style={{ fontSize: 23, color: '#317773', fontWeight: 'bold' }}>LOGIN/REGISTER</Text>
                <Text style={{ color: '#696969', fontSize: 17, marginTop: '3%' }}>Enter your mobile number to proceed</Text>
                <TextInput style={{ borderBottomWidth: 1, paddingBottom: 3, marginTop: '8%', fontSize: 20 }}
                    placeholder='10 digit mobile number/Email id'></TextInput>
            </View>
            <TouchableOpacity style={{
                marginTop: '10%', alignItems: 'center', backgroundColor: '#317773', width: '50%', height: '10%',
                justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:12}}
                onPress={() => Login()}>
                <Text style={{ fontSize: 23, color: 'white' }}>Login</Text>
            </TouchableOpacity>

            <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 2, marginTop: '10%' }}>
                <Text style={{ fontSize: 25 }}>CONTINUE WITH GOOGLE</Text>
            </View>

            <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 2, marginTop: '10%' }}>
                <Text style={{ fontSize: 25 }}>CONTINUE WITH FACEBOOK</Text>
            </View>

        </View>
    )
}
export default LoginPage;