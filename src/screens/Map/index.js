import React, { useEffect, useState } from 'react'
import { View, Text, Alert, TextInput, TouchableOpacity} from 'react-native'
import Geolocation from '@react-native-community/geolocation';
import MapView, { Marker } from 'react-native-maps';

const Map = () => {
  const [state, setState] = useState({
    region: {
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.97,
      longitudeDelta: 0.98,
    }
  })

  useEffect(() => {
    Geolocation.getCurrentPosition(
      position => {
        setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.97,
            longitudeDelta: 0.98,
          }
        });
      },
      (error) => {
        Alert.alert(error.message.toString());
      },
      {
        showLocationDialog: true,
        enableHighAccuracy: true,
        timeout: 20000
      },
    );

  })
  return (
    <View style={{ flex: 1, height: '50%' }}>
      <MapView
        style={{ height: '50%' }}
        showsUserLocation={true}
        region={state.region}
        onRegionChange={() => state.region}
      >
        <Marker
          coordinate={state.region}
        />
      </MapView>
      
        <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>

          <TouchableOpacity style={{ alignItems: 'center',  width: '30%', height: '10%',
                    justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:12}}>
          <Text style={{ fontSize: 23, color:'#317773' }}>Home</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{ alignItems: 'center',  width: '30%', height: '10%',
                    justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:12}}>       
              <Text style={{ fontSize: 23, color:'#317773' }}>Office</Text>
          </TouchableOpacity>

          <TouchableOpacity style={{ alignItems: 'center',  width: '30%', height: '10%',
                    justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:12}}>            
              <Text style={{ fontSize: 23, color:'#317773' }}>Others</Text>
          </TouchableOpacity>
        </View>
        <TextInput style={{borderWidth:1,width:'97%',alignSelf:'center',borderRadius:5,fontSize:18}}
           placeholder="Flat/House No.,Society">2
          </TextInput> 
          <TextInput style={{marginTop:'2%',borderWidth:1,width:'97%',alignSelf:'center',borderRadius:5,fontSize:18}}
           placeholder="Landmark">
          </TextInput>    

          <TouchableOpacity style={{marginTop: '32%', alignItems: 'center', backgroundColor: '#317773', width: '97%', height: '7%',
                    justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:10}}>
            <Text style={{ fontSize: 23, color: 'white' }}>Save Details</Text>
            </TouchableOpacity>  
    </View>
  )
}
export default Map;