import React from 'react';
import { TouchableOpacity, View ,Text,Image} from 'react-native';
const SelectAddress=({navigation})=>{
    const OnMap=()=>{
        navigation.navigate('Map')

    }
    return(
        <View style={{height:'100%',width:'100%'}}>
            <TouchableOpacity onPress={()=>OnMap()}>
            <View style={{flexDirection:'row',borderBottomWidth:2,height:'30%',marginTop:'5%'}}>
                <Image source={require('../../assets/currentLocation.png')}
                 style={{ height: 30, width: 30 }}></Image>
            <Text style={{fontSize:23}}>CHOOSE ON MAP</Text>
            </View>
            </TouchableOpacity>
            
        </View>
    )
}
export default SelectAddress;