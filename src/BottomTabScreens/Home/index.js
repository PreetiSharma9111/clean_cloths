import React from 'react';
import { useState } from 'react';
import { View, Image, ScrollView, Text, TouchableOpacity, FlatList } from 'react-native';
const Home = ({navigation}) => {
    const [clothType, setClothType] = useState([
        {Image:require('../../assets/wash&fold.png')},
        {Image:require('../../assets/laundry.png')},
        {Image:require('../../assets/organicwash.png')},
        {Image:require('../../assets/woolenwash.png')},
        {Image:require('../../assets/curtain.png')},
        {Image:require('../../assets/ironing.png')},
        {Image:require('../../assets/drycleaning.png')},
        {Image:require('../../assets/premiumdrycleaning.png')},
        {Image:require('../../assets/shose&bag.png')},
        {Image:require('../../assets/household.png')},
        {Image:require('../../assets/sofacleaning.png')},
        {Image:require('../../assets/Sanitization.png')},
        {Image:require('../../assets/pestcontrol.png')}
        
    ])
    const Proceed = () => {
        navigation.navigate('SelectAddress')
    }
    return (
        <View>
            <ScrollView>
            <FlatList
            data={clothType}
            numColumns={3}
            renderItem={({item,index})=>(
                <View key={index} style={{marginTop:'5%',alignItems:'center',justifyContent:'center',alignContent:'center'}}>
                    <TouchableOpacity onPress={()=>alert(index.valueOf())}>
                        <Image source={item.Image}
                        style={{height:100,width:100,margin:5,borderRadius:30}} resizeMode="contain"></Image>
                    </TouchableOpacity>
                </View>
            )}></FlatList>
            
            <TouchableOpacity onPress={()=>Proceed()}
            style={{
                    marginTop: '10%',marginBottom:'10%', alignItems: 'center', backgroundColor: '#317773', width: '50%', height: '10%',
                    justifyContent: 'center', alignItems: 'center', alignContent: 'center', alignSelf: 'center',borderRadius:12}}>
                    <Text style={{ fontSize: 23, color: 'white' }}>Proceed</Text>
            
            </TouchableOpacity>
</ScrollView>
        </View>
    )
}
export default Home;